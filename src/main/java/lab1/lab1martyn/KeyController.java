package lab1.lab1martyn;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;

public class KeyController {
    private Menu menu;
    public void setMenu(Menu newMenu) {
        menu = newMenu;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button buttonExport;

    @FXML
    private Button buttonExportSearch;

    @FXML
    private Button buttonImport;

    @FXML
    private Button buttonImportSearch;

    @FXML
    private Button buttonExportSearch1;

    @FXML
    private Button buttonDelete;

    @FXML
    private Button buttonConfirm;

    @FXML
    private Button buttonLogin;

    @FXML
    private TextField textFieldPathExp;

    @FXML
    private TextField textFieldPathImp;

    @FXML
    private TextField textFieldPathExp1;

    @FXML
    private TextField textFieldUser;

    @FXML
    private TextField textFieldUser1;

    @FXML
    private Label labelUser;

    @FXML
    private Label labelImport;

    @FXML
    private Label labelExport;

    @FXML
    private Label labelDelete;

    boolean delete = false;
    @FXML
    void onActionDelete(ActionEvent event) {
        labelDelete.setText("");
        if (!delete) {
            buttonConfirm.setDisable(false);
            buttonConfirm.setVisible(true);
            buttonDelete.setText("Отменить");

            labelDelete.setText("Вы уверены?");
            labelDelete.setTextFill(Paint.valueOf("RED"));
            labelDelete.setAlignment(Pos.TOP_RIGHT);
            delete = true;
        } else {
            buttonConfirm.setDisable(true);
            buttonConfirm.setVisible(false);
            buttonDelete.setText("Удалить свои ключи");
            delete = false;
        }
    }

    @FXML
    void onActionConfirm(ActionEvent event) {
        buttonConfirm.setDisable(true);
        buttonConfirm.setVisible(false);
        buttonDelete.setText("Удалить свои ключи");

        labelDelete.setText("Ключи удалены");
        labelDelete.setTextFill(Paint.valueOf("GREEN"));
        labelDelete.setAlignment(Pos.TOP_CENTER);

        deleteKeys(new File("users/".concat(textFieldUser.getText())));

        textFieldUser1.setText("");
        textFieldUser.setText("");
        textFieldPathExp.setText("");
        textFieldPathExp1.setText("");
        textFieldPathImp.setText("");

        textFieldPathExp1.setDisable(true);
        textFieldPathExp.setDisable(true);
        textFieldPathImp.setDisable(true);
        buttonDelete.setDisable(true);
        buttonImport.setDisable(true);
        buttonExport.setDisable(true);
        buttonExportSearch1.setDisable(true);
        buttonExportSearch.setDisable(true);
        buttonImportSearch.setDisable(true);

        delete = false;
    }

    public static void deleteKeys(File file) {
        if (!file.exists())
            return;

        if (file.isDirectory()) {
            for (File f : Objects.requireNonNull(file.listFiles())) {
                deleteKeys(f);
            }
        }

        file.delete();
    }

    @FXML
    void onActionExit(ActionEvent event) {
        menu.exit();
    }

    @FXML
    void onActionMain(ActionEvent event) {
        menu.openMainPage();
    }

    @FXML
    void onActionExport(ActionEvent event) {
        String user = textFieldUser.getText();
        String pathFrom = textFieldPathExp.getText();
        String pathTo = textFieldPathExp1.getText();

        labelImport.setText("");
        labelUser.setText("");
        labelExport.setText("");
        labelDelete.setText("");

        if (!menu.isUser(user)) {
            File file = new File("users/".concat(user));
            if (!file.exists()) {
                file.mkdir();
            }

            menu.keyGenEC(user);
            labelUser.setText("Пользователь не найден, создана новая пара ключей");
            labelUser.setAlignment(Pos.TOP_LEFT);
            labelUser.setTextFill(Paint.valueOf("BLACK"));
        }

        String userPathKeys = "users/".concat(user).concat("/").concat(user);
        PrivateKey prKey = menu.getCheckPrkKey(user, userPathKeys.concat(".prkEC"), "EC");
        PublicKey puKey = menu.getCheckPubKey(user, userPathKeys.concat(".pubEC"), "EC");

        if (prKey == null || puKey == null) {
            System.out.println("keys are broken");

            labelExport.setText("Что-то пошло не так с ключами");
            labelExport.setAlignment(Pos.TOP_CENTER);
            labelExport.setTextFill(Paint.valueOf("RED"));
            return;
        }

        if (pathFrom.equals("")) {
            System.out.println("pathFrom are broken");

            labelExport.setText("Неверный путь");
            labelExport.setAlignment(Pos.TOP_CENTER);
            labelExport.setTextFill(Paint.valueOf("RED"));
            return;
        }

        if (pathTo.equals("")) {
            System.out.println("pathTo are broken");

            labelExport.setText("Неверный путь");
            labelExport.setAlignment(Pos.TOP_CENTER);
            labelExport.setTextFill(Paint.valueOf("RED"));
            return;
        }

        File filePubKey = new File(pathFrom);

        try {
            byte[] data = Files.readAllBytes(Path.of(filePubKey.getPath()));

            int lenA = menu.convertByte(data[0]);
            int lenK = data.length - 2 - lenA;
            byte[] author = new byte[lenA];
            byte[] key = new byte[lenK];

            System.arraycopy(data, 2, author, 0, lenA);
            System.arraycopy(data, 2+lenA, key, 0, lenK);

            String nameAuthor = new String(author, StandardCharsets.UTF_8);
            textFieldUser1.setText(nameAuthor);

            // here
            if (!user.equals(nameAuthor)) {
                labelImport.setText("Ключ могут экспортировать только владельцы");
                labelImport.setAlignment(Pos.TOP_CENTER);
                labelImport.setTextFill(Paint.valueOf("RED"));
                return;
            }

            if (Character.compare(pathTo.charAt(pathTo.length()-1), '\\') != 0) {
                pathTo = pathTo.concat("\\");
            }
            menu.createPublic(key, nameAuthor, pathTo);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        labelExport.setText("Ключ успешно экспортирован");
        labelExport.setTextFill(Paint.valueOf("GREEN"));
        labelExport.setAlignment(Pos.TOP_CENTER);
    }

    @FXML
    void onActionExportSearch(ActionEvent event) {
        String user = textFieldUser.getText();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите подписанный документ");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Публичный ключ(.pub)", "*.pub"));
        File defaultDirectory = new File("./users/".concat(user));
        fileChooser.setInitialDirectory(defaultDirectory);
        File selectedFile = fileChooser.showOpenDialog(new Stage());

        if (selectedFile == null || !selectedFile.exists()) {
            return;
        }

        textFieldPathExp.setText(selectedFile.getPath());
    }

    @FXML
    void onActionExportSearch1(ActionEvent event) {
        String user = textFieldUser.getText();

        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Выберите папку для хранения");
        File defaultDirectory = new File("./publicKeys");

        if (!defaultDirectory.exists()) {
            defaultDirectory.mkdir();
        }

        chooser.setInitialDirectory(defaultDirectory);
        File selectedDirectory = chooser.showDialog(new Stage());

        if (selectedDirectory != null) {
            textFieldPathExp1.setText(selectedDirectory.getPath());
        }

        if (selectedDirectory == null || !selectedDirectory.exists()) {
            return;
        }

        textFieldPathExp1.setText(selectedDirectory.getPath());
    }

    @FXML
    void onActionImport(ActionEvent event) {
        String user = textFieldUser.getText();
        String pathFrom = textFieldPathImp.getText();
        String pathTo = "users/".concat(user).concat("/");

        labelImport.setText("");
        labelUser.setText("");
        labelExport.setText("");
        labelDelete.setText("");

        if (!menu.isUser(user)) {
            File file = new File(pathTo);
            if (!file.exists()) {
                file.mkdir();
            }

            menu.keyGenEC(user);
            labelUser.setText("Пользователь не найден, создана новая пара ключей");
            labelUser.setTextFill(Paint.valueOf("BLACK"));
            labelUser.setAlignment(Pos.TOP_LEFT);
        }

        pathTo = pathTo.concat("PK").concat("/");
        File file = new File(pathTo);
        if (!file.exists()) {
            file.mkdir();
        }

        String userPathKeys = "users/".concat(user).concat("/").concat(user);
        PrivateKey prKey = menu.getCheckPrkKey(user, userPathKeys.concat(".prkEC"), "EC");
        PublicKey puKey = menu.getCheckPubKey(user, userPathKeys.concat(".pubEC"), "EC");

        if (prKey == null || puKey == null) {
            labelImport.setText("Что-то не так с EC ключами пользователя");
            labelImport.setAlignment(Pos.TOP_CENTER);
            labelImport.setTextFill(Paint.valueOf("RED"));
            return;
        }

        if (pathFrom.equals("")) {
            labelImport.setText("Неверный путь");
            labelImport.setAlignment(Pos.TOP_CENTER);
            labelImport.setTextFill(Paint.valueOf("RED"));
            return;
        }

        if (pathTo.equals("")) {
            labelImport.setText("Что-то не так с путем");
            labelImport.setAlignment(Pos.TOP_CENTER);
            labelImport.setTextFill(Paint.valueOf("RED"));
            return;
        }

        try {
            byte[] data = Files.readAllBytes(Path.of(pathFrom));
            int len = menu.convertByte(data[0]);
            byte[] auth = new byte[len];
            System.arraycopy(data, 2, auth, 0, len);

            textFieldUser1.setText(new String(auth, Charset.defaultCharset()));
            pathTo = pathTo.concat(new String(auth, Charset.defaultCharset()));

            PublicKey publicKey = menu.getPubKey(user, pathFrom, "RSA");
            if (publicKey == null) {
                System.out.println("key ruined");

                labelImport.setText("Что-то не так с импортируемым ключом");
                labelImport.setAlignment(Pos.TOP_CENTER);
                labelImport.setTextFill(Paint.valueOf("RED"));
                return;
            }
            menu.createKeyDoc(publicKey.getEncoded(), user, new String(auth, Charset.defaultCharset()), "pub", pathTo, prKey);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        labelImport.setText("Ключ успешно импортирован");
        labelImport.setAlignment(Pos.TOP_CENTER);
        labelImport.setTextFill(Paint.valueOf("GREEN"));
    }

    @FXML
    void onActionImportSearch(ActionEvent event) {
        String user = textFieldUser.getText();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите ваш публичный ключ");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Публичный ключ(.public)", "*.public"));
        File defaultDirectory = new File("./publicKeys");
        fileChooser.setInitialDirectory(defaultDirectory);
        File selectedFile = fileChooser.showOpenDialog(new Stage());

        if (selectedFile == null || !selectedFile.exists()) {
            return;
        }

        textFieldPathImp.setText(selectedFile.getPath());
    }

    @FXML
    void onActionKeys(ActionEvent event) {

    }

    @FXML
    void onActionAbout(ActionEvent event) {
        menu.openAbout();
    }

    @FXML
    void onActionLogin(ActionEvent event) {
        String username = textFieldUser.getText();

        if (username.equals("")) {
            textFieldPathExp.setDisable(true);
            textFieldPathImp.setDisable(true);
            textFieldPathExp1.setDisable(true);

            buttonExport.setDisable(true);
            buttonExportSearch.setDisable(true);
            buttonImport.setDisable(true);
            buttonImportSearch.setDisable(true);
            buttonExportSearch1.setDisable(true);

            labelDelete.setText("");
            buttonDelete.setText("Удалить свои ключи");
            buttonDelete.setDisable(true);
            buttonConfirm.setDisable(true);
            delete = false;

            return;
        }

        File usersFolder = new File("users");
        if (!usersFolder.exists()) {
            usersFolder.mkdir();
        }

        ArrayList<String> Users = menu.getUsers();
        String userKeys = "users/".concat(username).concat("/").concat(username);


        if (Users.contains(username)) {
            if (!menu.checkKeys(username, userKeys.concat(".pub")) ||
                    !menu.checkKeys(username, userKeys.concat(".prk"))) {

                menu.keyGenRSA(username);
                if (!menu.checkKeys(username, userKeys.concat(".pub")) ||
                        !menu.checkKeys(username, userKeys.concat(".prk"))) {
                    return;
                }
            }
        } else {

            File folder = new File(usersFolder.getName().concat("/").concat(username));
            if (!folder.exists()) {
                if (!folder.mkdir()) {
                    System.out.println("impossible...");
                } else {
                    menu.keyGenRSA(username);
                }
            }
        }

        if (!menu.checkKeys(username, userKeys.concat(".pub")) || !menu.checkKeys(username, userKeys.concat(".prk"))) {
            System.out.println("keys not created!");

            return;
        }

        textFieldPathExp.setDisable(false);
        textFieldPathImp.setDisable(false);
        textFieldPathExp1.setDisable(false);

        buttonExport.setDisable(false);
        buttonExportSearch.setDisable(false);
        buttonImport.setDisable(false);
        buttonImportSearch.setDisable(false);
        buttonExportSearch1.setDisable(false);

        labelDelete.setText("");
        buttonDelete.setText("Удалить свои ключи");
        buttonDelete.setDisable(false);
        buttonConfirm.setDisable(true);
        delete = false;
    }

    @FXML
    void initialize() {
        assert buttonExport != null : "fx:id=\"buttonExport\" was not injected: check your FXML file 'key-page.fxml'.";
        assert buttonExportSearch != null : "fx:id=\"buttonExportSearch\" was not injected: check your FXML file 'key-page.fxml'.";
        assert buttonImport != null : "fx:id=\"buttonImport\" was not injected: check your FXML file 'key-page.fxml'.";
        assert buttonImportSearch != null : "fx:id=\"buttonImportSearch\" was not injected: check your FXML file 'key-page.fxml'.";
        assert buttonLogin != null : "fx:id=\"buttonLogin\" was not injected: check your FXML file 'key-page.fxml'.";
        assert textFieldPathExp != null : "fx:id=\"textFieldPathExp\" was not injected: check your FXML file 'key-page.fxml'.";
        assert textFieldPathImp != null : "fx:id=\"textFieldPathImp\" was not injected: check your FXML file 'key-page.fxml'.";
        assert textFieldUser != null : "fx:id=\"textFieldUser\" was not injected: check your FXML file 'key-page.fxml'.";
        assert textFieldUser1 != null : "fx:id=\"textFieldUser1\" was not injected: check your FXML file 'key-page.fxml'.";

    }

}
