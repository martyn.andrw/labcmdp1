package lab1.lab1martyn;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("file-page.fxml"));

        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Сохранение и загрузка подписанных документов");
        stage.setScene(scene);
        stage.show();

        Menu menu = new Menu(stage);
        FileController fileController = fxmlLoader.getController();
        fileController.setMenu(menu);
    }

    public static void main(String[] args) {
        launch();
    }
}