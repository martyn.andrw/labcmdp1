package lab1.lab1martyn;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FileController {
    private Menu menu;
    public void setMenu(Menu newMenu) {
        menu = newMenu;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button buttonLoad;

    @FXML
    private Button buttonLogin;

    @FXML
    private Button buttonPath;

    @FXML
    private Button buttonSave;

    @FXML
    private TextArea textArea;

    @FXML
    private TextField textFieldPath;

    @FXML
    private TextField textFieldUser;

    @FXML
    private TextField textFieldAuthor;

    @FXML
    private TextField textFieldFileName;

    @FXML
    private Label labelErrorLoad;

    @FXML
    private Label labelErrorSave;

    @FXML
    private Label labelErrorUser;

    @FXML
    void onActionExit(ActionEvent event) {
        menu.exit();
    }

    @FXML
    void onActionKeys(ActionEvent event) {
        menu.openKeysPage();
    }

    @FXML
    void onActionMain(ActionEvent event) { }

    @FXML
    void onActionAbout(ActionEvent event) {
        menu.openAbout();
    }

    @FXML
    void onActionLoad(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите подписанный документ");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Подписанный документ(.sd)", "*.sd"));
        File defaultDirectory = new File("./");
        fileChooser.setInitialDirectory(defaultDirectory);
        File selectedFile = fileChooser.showOpenDialog(new Stage());

        textArea.setText("");
        textFieldPath.setText("");
        textFieldAuthor.setText("");
        textFieldFileName.setText("");
        labelErrorLoad.setText("");
        labelErrorSave.setText("");
        labelErrorLoad.setText("");

        if (selectedFile == null) {
            return;
        }

        String pathTF = selectedFile.getPath();
        pathTF = pathTF.substring(0, pathTF.lastIndexOf('\\'));
        textFieldPath.setText(pathTF);
        textFieldFileName.setText(selectedFile.getName());
        try {
            byte[] data = Files.readAllBytes(Path.of(selectedFile.getPath()));
            int lenAuthor = menu.convertByte(data[0]);
            int lenSign = menu.convertByte(data[1]);
            int lenText = data.length - 2 - lenAuthor - lenSign;

            byte[] author = new byte[lenAuthor];
            byte[] text = new byte[lenText];

            System.arraycopy(data, 2, author, 0, lenAuthor);
            System.arraycopy(data, 2+lenAuthor+lenSign, text, 0, lenText);

            String usernameA = new String(author, StandardCharsets.UTF_8);
            String username = textFieldUser.getText();
            textFieldAuthor.setText(usernameA);

            String path = "users/".concat(username).concat("/").concat(usernameA).concat(".pub");

            if (!usernameA.equals(username)) {
                PublicKey publicKey = menu.findPubKey(username, usernameA);
                if (publicKey == null) {
                    labelErrorLoad.setText("Ключ не найден или поврежден");
                    labelErrorLoad.setAlignment(Pos.CENTER);
                    labelErrorLoad.setTextFill(Paint.valueOf("RED"));
                    return;
                }
                if (menu.checkSignedDoc(selectedFile.getPath(), publicKey)) {
                    textArea.setText(new String(text, StandardCharsets.UTF_8));
                } else {
                    textArea.setText("");
                    textFieldPath.setText("");
                    textFieldFileName.setText("");

                    labelErrorLoad.setText("Файл изменен или поврежден!");
                    labelErrorLoad.setAlignment(Pos.TOP_CENTER);
                    labelErrorLoad.setTextFill(Paint.valueOf("RED"));
                }
            } else {
                if (menu.checkSignedDoc(selectedFile.getPath(), menu.getPubKey(username, path, "RSA"))) {
                    textArea.setText(new String(text, StandardCharsets.UTF_8));
                } else {
                    textArea.setText("");
                    textFieldPath.setText("");
                    textFieldFileName.setText("");

                    labelErrorLoad.setText("Файл изменен или поврежден!");
                    labelErrorLoad.setAlignment(Pos.TOP_CENTER);
                    labelErrorLoad.setTextFill(Paint.valueOf("RED"));
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    void onActionLogin(ActionEvent event) {
        String username = textFieldUser.getText();

        labelErrorUser.setText("");
        labelErrorUser.setTextFill(Paint.valueOf("BLACK"));
        labelErrorUser.setAlignment(Pos.CENTER);
        textFieldFileName.setText("");
        textFieldPath.setText("");
        textArea.setText("");

        if (username.equals("")) {
            textArea.setDisable(true);
            textFieldPath.setDisable(true);
            buttonSave.setDisable(true);
            textFieldPath.setDisable(true);
            buttonPath.setDisable(true);
            textFieldFileName.setDisable(true);
            buttonLoad.setDisable(true);

            labelErrorUser.setText("Поле не может быть пустым!");
            labelErrorUser.setTextFill(Paint.valueOf("RED"));
            labelErrorUser.setAlignment(Pos.TOP_LEFT);
            return;
        }

        File usersFolder = new File("users");
        if (!usersFolder.exists()) {
            usersFolder.mkdir();
        }

        ArrayList<String> Users = menu.getUsers();
        String userKeys = "users/".concat(username).concat("/").concat(username);


        if (Users.contains(username)) {
            if (!menu.checkKeys(username, userKeys.concat(".pub")) ||
                    !menu.checkKeys(username, userKeys.concat(".prk"))) {
                menu.keyGenRSA(username);
                if (!menu.checkKeys(username, userKeys.concat(".pub")) ||
                        !menu.checkKeys(username, userKeys.concat(".prk"))) {
                    return;
                }
            }
        } else {
            labelErrorUser.setText("Пользователь не найден, создана новая пара ключей");
            labelErrorUser.setTextFill(Paint.valueOf("BLACK"));
            labelErrorUser.setAlignment(Pos.TOP_LEFT);

            File folder = new File(usersFolder.getName().concat("/").concat(username));
            if (!folder.exists()) {
                if (!folder.mkdir()) {
                    System.out.println("impossible...");
                } else {
                    menu.keyGenRSA(username);
                }
            }
        }

        if (!menu.checkKeys(username, userKeys.concat(".pub")) || !menu.checkKeys(username, userKeys.concat(".prk"))) {
            System.out.println("keys not created!");

            labelErrorUser.setText("Отсутвуют ключи, попробуйте пересоздать пользователя");
            labelErrorUser.setTextFill(Paint.valueOf("RED"));
            labelErrorUser.setAlignment(Pos.TOP_LEFT);
            return;
        }

        textArea.setDisable(false);
        textFieldPath.setDisable(false);
        buttonSave.setDisable(false);
        textFieldPath.setDisable(false);
        buttonPath.setDisable(false);
        textFieldFileName.setDisable(false);
        buttonLoad.setDisable(false);
    }

    @FXML
    void onActionPath(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Выберите папку для хранения");
        File defaultDirectory = new File("./");
        chooser.setInitialDirectory(defaultDirectory);
        File selectedDirectory = chooser.showDialog(new Stage());

        if (selectedDirectory != null) {
            textFieldPath.setText(selectedDirectory.getPath());
        }
    }

    @FXML
    void onActionSave(ActionEvent event) {
        String path = textFieldPath.getText();
        String filename = textFieldFileName.getText();
        String username = textFieldUser.getText();
        String userKeys = "users/".concat(username).concat("/").concat(username);

        labelErrorSave.setText("");
        labelErrorLoad.setText("");

        if (filename.equals("")) {
            labelErrorSave.setText("Наименование файла не может быть пустым!");
            labelErrorSave.setTextFill(Paint.valueOf("RED"));
            labelErrorSave.setAlignment(Pos.CENTER);

            return;
        }
        if (!filename.contains(".sd")) {
            filename = filename.concat(".sd");
        }

        String finalPath = "";
        if (!path.equals("")) {
            finalPath = finalPath.concat(path);
            if (finalPath.charAt(finalPath.length() - 1) != '/') {
                finalPath = finalPath.concat("/");
            }
        }

        finalPath = finalPath.concat(filename);

        File file = new File(finalPath);
        if (file.exists()) {
            String au = textFieldAuthor.getText();
            if (!au.equals("") && !au.equals(username)) {
                labelErrorSave.setText("Невозможно изменить подписанный документ.");
                labelErrorLoad.setText("Пользователь не является автором!");

                labelErrorSave.setTextFill(Paint.valueOf("RED"));
                labelErrorSave.setAlignment(Pos.CENTER);

                labelErrorLoad.setTextFill(Paint.valueOf("RED"));
                labelErrorLoad.setAlignment(Pos.CENTER);
                return;
            }
        }

        if (!menu.signDoc(textFieldUser.getText(), textArea.getText(), finalPath)) {
            System.out.println("not signed");

            labelErrorSave.setText("Файл не подписан, возможно изменен приватный ключ");
            labelErrorSave.setTextFill(Paint.valueOf("RED"));
            labelErrorSave.setAlignment(Pos.CENTER);

            return;
        } else {
            System.out.println("signed");
        }

        if (!menu.checkSignedDoc(finalPath, menu.getCheckPubKey(username, userKeys.concat(".pub"), "RSA"))) {
            System.out.println("not verified");

            labelErrorSave.setText("Файл не верифицирован, возможно изменена пара ключей");
            labelErrorSave.setTextFill(Paint.valueOf("RED"));
            labelErrorSave.setAlignment(Pos.CENTER);

            return;
        } else {
            System.out.println("verified");
            labelErrorSave.setText("Файл успешно подписан");
            labelErrorSave.setTextFill(Paint.valueOf("GREEN"));
            labelErrorSave.setAlignment(Pos.CENTER);

            textFieldFileName.setText("");
            textFieldPath.setText("");
            textArea.setText("");
        }
    }

    @FXML
    void initialize() {
        assert buttonLoad != null : "fx:id=\"buttonLoad\" was not injected: check your FXML file 'file-page.fxml'.";
        assert buttonLogin != null : "fx:id=\"buttonLogin\" was not injected: check your FXML file 'file-page.fxml'.";
        assert buttonPath != null : "fx:id=\"buttonPath\" was not injected: check your FXML file 'file-page.fxml'.";
        assert buttonSave != null : "fx:id=\"buttonSave\" was not injected: check your FXML file 'file-page.fxml'.";
        assert labelErrorLoad != null : "fx:id=\"labelErrorLoad\" was not injected: check your FXML file 'file-page.fxml'.";
        assert labelErrorSave != null : "fx:id=\"labelErrorSave\" was not injected: check your FXML file 'file-page.fxml'.";
        assert labelErrorUser != null : "fx:id=\"labelErrorUser\" was not injected: check your FXML file 'file-page.fxml'.";
        assert textArea != null : "fx:id=\"textArea\" was not injected: check your FXML file 'file-page.fxml'.";
        assert textFieldAuthor != null : "fx:id=\"textFieldAuthor\" was not injected: check your FXML file 'file-page.fxml'.";
        assert textFieldFileName != null : "fx:id=\"textFieldFileName\" was not injected: check your FXML file 'file-page.fxml'.";
        assert textFieldPath != null : "fx:id=\"textFieldPath\" was not injected: check your FXML file 'file-page.fxml'.";
        assert textFieldUser != null : "fx:id=\"textFieldUser\" was not injected: check your FXML file 'file-page.fxml'.";

    }

}