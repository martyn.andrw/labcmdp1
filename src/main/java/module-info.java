module lab1.lab1martyn {
    requires javafx.controls;
    requires javafx.fxml;
    requires bcprov.jdk15;


    opens lab1.lab1martyn to javafx.fxml;
    exports lab1.lab1martyn;
}