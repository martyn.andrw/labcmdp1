jar файл запускается на JDK 18

Ключи для подписей генерируются в menu.keyGenRSA для RSA, menu.keyGenEC для ECDSA
Ключи сохраняются в файлы соответсвующих расширений ".pub" - публичный для RSA, ".prk" - приватный для RSA,
	".pubEC" - публичный для ECDSA, ".prkEC" - приватный для ECDSA
Сохрание ключей происходит в функции menu.createKeyDoc
Проверка подписи ключей проводится в функции menu.checkKeys, menu.getCheckPubKey, menu.getCheckPrkKey

Подпись текстового документа осуществляется в menu.signDoc
Проверка подписи текстого документа проводится в menu.checkSignedDoc

Экспортируется публичный ключ с помощью функции menu.createPublic, ".public" - экспортируемый публичный ключ
